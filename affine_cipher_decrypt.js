/*
Here's the code:
E(x) = (ax + b) mod m
*/
//Here goes....
var order = "abcdefghijklmnopqrstuvwxyz".split("");
console.log(order.length);
var stdin = process.openStdin();
console.log("Enter key A: ");
stdin.addListener("data", function(d) {
    d = d.toString().trim();
    console.log("Enter key B: ")
    stdin.addListener("data", function(data) {
        data = data.toString().trim();
        console.log("Now enter your string: ");
        stdin.addListener("data", function(da) {
            da = da.toString().trim();
            console.log(affine_cipher_d(d, data, da));
            process.exit();
        });
    });
});
function affine_cipher_d(a, b, x) {
    //To decrypt: c(x-b)
    //c = ax = 1 mod m
    //x is such that (a * x) mod m = 1
    //1) calc c
    x = x.split("");
    for(i in x) {
        for(ii in order) {
            if(order[ii] == x[i]) {
                x[i] = Number(ii);
            }
        }
    }
    var c = 0;
    for(var i = 0; i < order.length; i++) {
        //Check what x should be
        if((i * a) % order.length == 1) {
            c = i;
        }
    }
    var result = [];
    for(i in x) {
        result.push(order[c*(x[i]-b)]);
    }
    return result.join("");
}