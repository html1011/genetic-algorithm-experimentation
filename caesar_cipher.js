/*
Caesar cipher - fixed # of pos down alphabet
*/
function cipher(num_to_shift, input) {
    var order = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!.123456789, ".split("");
    num_to_shift = num_to_shift % order.length;
    input = input.split("");
    var output = [];
    if (num_to_shift > 0) {
        var mycount = num_to_shift;
        for (var i = num_to_shift; i < order.length + num_to_shift; i++) {
            if (order[mycount] == undefined) {
                mycount = 0;
            }
            output.push(order[mycount]);
            mycount++;
        }
    }
    else {
        var mycount = (order.length - 1) + num_to_shift;
        for (var i = (order.length - 1) + num_to_shift; i >= num_to_shift; i--) {
            if (order[mycount] == undefined) {
                mycount = order.length - 1;
            }
            output.push(order[mycount]);
            mycount--;
        
        }
        output = output.reverse();
    }
    //Find the position of the letters in the alphabet.
    var pos = [];
    for (i in input) {
        for (ii in order) {
            if (order[ii] == input[i]) {
                pos.push(ii);
            }
        }
    }
    var result = [];
    for (i in pos) {
        result.push(output[pos[i]]);
    }
    return result.join("");
}
var stdin = process.openStdin();
console.log("Enter the shift number: ");
stdin.addListener("data", function(d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that  
    // with toString() and then trim() 
    var data = d.toString().trim();
    console.log("Ok, now enter the input: ");
    stdin.addListener("data", function(da) {
        console.log("Calculating data....");
        console.log(cipher(Number(data), da.toString().trim()));
        stdin = process.exit();
    });
  });