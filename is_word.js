/*Check whether a word is real.*/
// var fs = require("fs");
// check("acculturize accusal");
// function check(ans) {
//     ans = ans.split(" ");
//     fs.readFile("./words.txt", "utf8", function (err, data) {
//         if (err) {
//             throw err;
//         }
//         //console.log(data);
//         data = data.split("\n");
//         var i = 0;
//         var num_words_found = 0;
//         var words_i_found = [];
//         var c = setInterval(function() {
//             console.log(i/(data.length-1) + "% through analyzing..... So far we have found " + num_words_found + " words: " + words_i_found);
//             if(ans.length == 0) {
//                 console.log("YES! I AM REAL!");
//                 clearInterval(c);
//             }
//             if(data[i] == undefined) {
//                 console.log("Sorry to disappoint you, but I unfortunately am not composed of real words.");
//                 clearInterval(c);
//             }
//             for(ii in ans) {
//                 if(ans[ii] == data[i]) {
//                     //It's a word!
//                     num_words_found++;
//                     words_i_found.push(ans[ii]);
//                     ans.splice(ii, 1);
//                 }
//             }
//             i++;
//         }, 1);
//     })
// }
var fs = require("fs");
var order = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!.123456789, ".split("");
var check = "Hey, wassup, Im going to use some lesserknown, slang type words in the hope that you still work, bro.";
checker(check);
function checker(ans) {
    var special_char = "!?.,".split("");
    var an = ans.split("");
    for(i in an) {
        for(ii in special_char) {
            if(special_char[ii] == an[i]) {
                an.splice(i, 1);
            }
        }
    }
    ans = an.join("");
    ans = ans.toLowerCase();
    ans = ans.split(" ");
    fs.readFile("./words.txt", "utf8", function (err, data) {
        if (err) {
            throw err;
        }
        //console.log(data);
        data = data.toLowerCase();
        data = data.split("\n");
        var num_words_found = 0;
        var words_i_found = [];
        for(var iii = 0; iii < data.length; iii++) {
            //console.log((iii/(data.length-1)*100) + "% through analyzing..... So far we have found " + num_words_found + " words: " + words_i_found);
            
            for(ii in ans) {
                if(ans[ii] == data[iii]) {
                    //It's a word!
                    num_words_found++;
                    words_i_found.push(ans[ii]);
                    ans.splice(ii, 1);
                    console.log(iii/(data.length-1)*100 + "% through analyzing..... So far we have found " + num_words_found + " words: " + words_i_found);
                }
            }
        }
        if(ans.length == 0) {
            console.log("YES! I AM REAL!");
        }
        else if(ans.length == 1) {
            console.log("Unfortunately the English dictionary does not consider this a word: " + ans);
        }
        else {
            console.log("Unfortunately the English dictionary does not consider these words: " + ans);
        }
    })
}