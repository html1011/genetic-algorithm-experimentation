var fs = require("fs");
function cipher(num_to_shift, input) {
    var order = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!.123456789, ".split("");
    num_to_shift = num_to_shift % order.length;
    input = input.split("");
    var output = [];
    if (num_to_shift > 0) {
        var mycount = num_to_shift;
        for (var i = num_to_shift; i < order.length + num_to_shift; i++) {
            if (order[mycount] == undefined) {
                mycount = 0;
            }
            output.push(order[mycount]);
            mycount++;
        }
    }
    else {
        var mycount = (order.length - 1) + num_to_shift;
        for (var i = (order.length - 1) + num_to_shift; i >= num_to_shift; i--) {
            if (order[mycount] == undefined) {
                mycount = order.length - 1;
            }
            output.push(order[mycount]);
            mycount--;
        
        }
        output = output.reverse();
    }
    //Find the position of the letters in the alphabet.
    var pos = [];
    for (i in input) {
        for (ii in order) {
            if (order[ii] == input[i]) {
                pos.push(ii);
            }
        }
    }
    var result = [];
    for (i in pos) {
        result.push(output[pos[i]]);
    }
    return result.join("");
}
console.log(`Uh oh! You forgot your friend's code! No matter, just give me your friend's code and I'll try my best to find the key(and the answer).
Input: `);
var keep = [];
var we_stopped = false;
var times_run = 0;
var stdin = process.openStdin();
stdin.addListener("data", function(d) {
    var input = d.toString().trim();
    var order = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!.123456789, ".split("");
    for(var i = 0; i > -order.length; i--) {
        var check = cipher(i, input);
        checker(check, i);
    }
    var f = setInterval(function() {
        if(we_stopped && times_run == order.length) {
            //The program's now terminated and we can read keep.
            for(i in keep) {
                keep[i][0] = keep[i][0].length;
                keep[i][1] = keep[i][1].split(" ");
            }
            keep = keep.sort(function(a, b) { return b[1].length-a[1].length });
            console.log(`Our best guess is that the code is "${keep[0][1].join(" ")}", with a key of ${keep[0][2]}.`);
            clearInterval(f);
        }
    }, 100);
    //process.exit();
});
/*Check whether a word is real.*/
function checker(ans, secret_key) {
    var special_char = "!?.,".split("");
    var an = ans.split("");
    for(i in an) {
        for(ii in special_char) {
            if(special_char[ii] == an[i]) {
                an.splice(i, 1);
            }
        }
    }
    var remember_me = ans.slice();
    ans = an.join("");
    ans = ans.toLowerCase();
    ans = ans.split(" ");
    fs.readFile("./words.txt", "utf8", function (err, data) {
        if (err) {
            throw err;
        }
        //console.log(data);
        data = data.toLowerCase();
        data = data.split("\n");
        var num_words_found = 0;
        var words_i_found = [];
        for(var iii = 0; iii < data.length; iii++) {
            //console.log((iii/(data.length-1)*100) + "% through analyzing..... So far we have found " + num_words_found + " words: " + words_i_found);
            
            for(ii in ans) {
                if(ans[ii] == data[iii]) {
                    //It's a word!
                    num_words_found++;
                    words_i_found.push(ans[ii]);
                    ans.splice(ii, 1);
                    console.log(iii/(data.length-1)*100 + "% through analyzing..... So far we have found " + num_words_found + " words: " + words_i_found);
                }
            }
        }
        if(ans.length == 0) {
            console.log("YES! I AM REAL!");
        }
        else if(ans.length == 1) {
            console.log("Unfortunately the English dictionary does not consider this a word: " + ans);
        }
        else {
            console.log("Unfortunately the English dictionary does not consider these words: " + ans);
        }
        keep.push([ans, remember_me, secret_key]);
        we_stopped = true;
        times_run++;
    })
}