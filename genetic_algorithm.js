/*Ok, here's the thing.
There is a reason us humans don't mate with our parents or sisters/brothers: a, 'cause it's weird, and b, because we won't
have enough genetic variation. So we need to do a lotta things here:
a) Start off with a bigger population.
b) That population then will become rated by fitness level(which can be whatever I want it to be)
c) Now that it's ranked by fitness level, we're going to have a little fight.
    Two "men" will fight for a "women"(since these are no-sex organisms you can just pick whoever).
    Whoever has the highest fitness rate survives and creates an offspring with the women.
    The other man dies.
    The other two survive to the next generation.
d) About that fitness level....
I'm gonna make my peas be homogenous, and have all dominent traits. So the perfect little pea would be:
    1) Smooth
    2) Yellow
    3) Purple
    4) Inflated
    5) Green
    6) Axial(on the tips)
    7) Tall
This is going to be in an array because I want to be able to change it and then I can add weights, eg. smoothness is more important than Inflatedness
*/
//Let's start off with listing the pea traits.
/*Form of ripe seed (R) – smooth or wrinkled
Color of seed albumen (Y) – yellow or green
Color of flower (P) – purple or white
Form of ripe pods (I) – inflated or constricted
Color of unripe pods (G) – green or yellow
Position of flowers (A) – axial or terminal
Length of stem (T) – tall or dwarf
 */
//1) Create two parents with completely random genotypes.
var traits = [
    ["smooth", "wrinkled"],
    ["yellow", "green"],
    ["purple", "white"],
    ["inflated", "constricted"],
    ["green", "yellow"],
    ["axial", "terminal"],
    ["tall", "dwarf"]
];
var perfect_pea = [
    ["RR", 0.7],
    ["yy", 0.3],
    ["PP", 0.9],
    ["II", 0.1],
    ["GG", 0.4],
    ["AA", 0.6],
    ["TT", 0.3]
];
//Now we calculate the maximum fitness and divide perfect_pea[1] by that.
var max_fit = 0;
for(i in perfect_pea) {
    max_fit += (perfect_pea[i][0].split("").length) * perfect_pea[i][1];
}
for(i in perfect_pea) {
    perfect_pea[i][1] /= max_fit;
}
var dominent = ("RYPIGAT").split(""); //Listing all the dominent traits
var recessive = [];
for (i in dominent) {
    recessive.push(dominent[i].toLowerCase());
}
//Now, we need to pick and choose.
function create(pair) {
    var result = [];
    for(var ii = 0; ii < pair; ii++) {
var parenta = dominent.slice();
for (i in dominent) {
    //Ok. 50/50 chance of getting recessive or dominent.
    var chance = Math.random();
    if (chance <= 0.5) {
        //Recessive; For now we're making there be a higher chance
        parenta[i] = [recessive[i]];
    }
    else {
        parenta[i] = [dominent[i]];
    }
    var chanceaa = Math.random();
    if (chanceaa < 0.5) {
        //Recessive
        parenta[i].push(recessive[i]);
    }
    else {
        parenta[i].push(dominent[i]);
    }
    }
    result.push(parenta);
}
    return result;
}
var parents = create(500);
console.log(parents.length);
//That worked pretty well. Now we need to put the dominent traits in front, just for formatting purposes.
for (i in parents) {
    for(var ii in parents[i]) {
        parents[i][ii] = parents[i][ii].sort();
    }
}
//Hmmmmmm..... Now let's figure out what traits should show up, just for fun.
function trait_finder(a) {
var traits_for_a = [];
for (i in a) {
    if (a[i][0] == a[i][0].toUpperCase()) {
        //Dominent trait shows up
        //We're not going to do incomplete dominence for now, because then this could get a little messy.
        traits_for_a.push(traits[i][0]);
    }
    else {
        traits_for_a.push(traits[i][1]);
    }
}
return traits_for_a;
}
var traits_for_parents = [];
for(i in parents) {
        traits_for_parents.push(trait_finder(parents[i]));
}
//Okay. Now for the child.
//There's crossover and mutation to consider, meaning that we're going to use crossover to create the child, then we can set a mutation probability.
function create_child(parenta, parentb) {
    var child_result = [];
var mutation_probability = 0.01; //Probably something low
//The human mutation probability is 0.032ish, so let's use that.
//We have 7 traits to consider, and we should find a crossing point. For now, let's do it as close to the middle as possible.
var crossover_point = 3;
var child = [];
    //Ok. We're going to create a Punett square.
    /*
    Let's think:
      Y   y
    y Yy  yy
    y Yy  yy
    There's a 1/4 chance of getting each.
    */
   for(i in parenta) {
       //Let's draw it out because why not?
//        console.log(`Parenta: ${parenta[i]},
// Parentb: ${parentb[i]}
//    ${parenta[i][0]}   ${parenta[i][1]}
// ${parentb[i][0]}  ${parenta[i][0]}${parentb[i][0]}  ${parenta[i][1]}${parentb[i][0]}
// ${parentb[i][1]}  ${parenta[i][0]}${parentb[i][1]}  ${parenta[i][1]}${parentb[i][1]}
// `);
//I know, I know, it's not sorted properly but it's just a model.
        //Even though al the genotypes we have here are only 2 long, I'm gonna get prepared for fancy stuff like blood types, in the event of something cool happening.
        var our_chances = [];
        for(ii in parenta[i]) {
            for(z in parentb[i]) {
                var chance = [parenta[i][ii], parentb[i][z]];
                our_chances.push(chance.sort());
            }
        }
        //Now we choose a random one from our_chances, and that's our child.
        child_result.push(our_chances[Math.floor(Math.random()*our_chances.length)])
   }
    //Now the mutaaaaation!
    for(i in child_result) {
        if(Math.random() <= mutation_probability) {
            //This means it mutates
            /*There are 3 types of mutations:
             heterozygous mutation - mutation of 1 allele
             homozygous mutation - identical mutation of 2 alleles
             Compound heterozygous mutations - 2 different mutations of 2 alleles
            */
           var chances = Math.random()*3;
           if(chances < 1) {
               //Heterozygous mutation
               if(child_result[i][0] == child_result[i][0].toLowerCase()) {
                   child_result[i][0] = child_result[i][0].toUpperCase();
               }
               else {
                   child_result[i][0] = child_result[i][0].toLowerCase();
               }
           }
           else if(chances < 2) {
               //Homozygous mutation
                if(child_result[i][0] == child_result[i][0].toLowerCase()){
                    child_result[i][0] = child_result[i][0].toUpperCase();
                    child_result[i][1] = child_result[i][0].toUpperCase(); 
                }
                else {
                    child_result[i][0] = child_result[i][0].toLowerCase();
                    child_result[i][1] = child_result[i][0].toLowerCase();
                }
           }
           else {
               //Meh, don't feel like it.
               //Fiiiine, I'll do it. One Compound heterozgous mutation coming up.
                if(child_result[i][0] == child_result[i][0].toLowerCase()) {
                    child_result[i][0] = child_result[i][0].toUpperCase();
                    child_result[i][1] = child_result[i][0].toLowerCase();
                }
                else {
                    child_result[i][0] = child_result[i][0].toLowerCase();
                    child_result[i][1] = child_result[i][0].toUpperCase();
                }
           }
           //We need to sort all the stuff again, because we will have messed it all up now.
        }
    }
    for(i in child_result) {
        child_result[i].sort();
    }
    //This could go terribly wrong.... but I truely hope not.
    return [child_result, trait_finder(child_result)];
}

function fitness(list) {
    var results = 0;
    /*Input is 1(ONE) thing. Then we calc its fitness. */
    for(i in list) {
        var li = perfect_pea[i][0].split("");
        for(ii in li) {
            if(list[i][ii] == perfect_pea[i][0][ii]) {
                results += perfect_pea[i][1];
            }
        }
    }
    return results;
}
//Ok, now we've got the fitness
//1) Choose 3 random people from parents

function shuffle(array) {
    var i = array.length,
        j = 0,
        temp;

    while (i--) {

        j = Math.floor(Math.random() * (i+1));

        // swap randomly chosen element with current element
        temp = array[i];
        array[i] = array[j];
        array[j] = temp;

    }
    return array;
}
function fight(people) {
    var woman = people[0];
    var mana = people[1];
    var manb = people[2];
    var survivors = [woman];
    //console.log(survivors);
    //She automatically survives.
    //Now mana and manb fight for her.
    var loser = [];
    if(fitness(mana) > fitness(manb)) {
        survivors.push(mana); /*Ha! Man a, you win! */
        loser.push(manb);
    }
    else if(fitness(manb) > fitness(mana)) {
        survivors.push(manb); /*Ha! Man b, you win! */
        loser.push(mana);
    }
    else {
        survivors.push(manb); /*Ha! Man b, you win(even if you guys are the same)! */
        loser.push(mana);
    }
    /*Now whoever's not a survivor dies. Sorry! */
    return [survivors, loser];
}
var times = 0;
for(i in parents) {
    console.log("Generation 1: " + trait_finder(parents[i]));
}
//Ok. Let's take this one step at a time.
/*
1) 3 people need to" get into our little fighting-thing.
Well, 3 peas.
Anyways.....
2) Then we kill the loser and create a baby with the winners.
3) Since I don't want my head to explode, we're only going to do one offspring per generation.
*/
var num_generation = 1;
function generations() {
    num_generation++;
    for(var ii = 0; ii < 100; ii++) {
        var choose = shuffle(parents).slice(0, 3); //We've chosen the fighters now.
        var winners = fight(choose)[0];
        var loser = fight(choose)[1][0];
        //Now we have the winner and the loser.
        //We get rid of the loser now.
        for(i in parents) {
            if(parents[i] == loser) {
                parents.splice(i, 1);
            }
        }
        parents.push(create_child(winners[0], winners[1])[0]);
    }
    for(i in parents) {
        var trait_list = trait_finder(parents[i]);
        console.log(`Generation ${num_generation} : ${trait_list}`);
    }
}
setInterval(generations, 100);