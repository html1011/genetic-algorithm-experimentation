var stdin = process.openStdin();
var fs = require("fs");
stdin.addListener("data", function(d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that  
    // with toString() and then trim() 
    fs.readFile(d.toString().trim(), "utf8", function(err, data) {
        if(err) throw err;
        var F=new Function (data);
        return(F());
    });
  });